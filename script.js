const books = [{
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  }
];


function addList (books) {
  let root = document.createElement('div');
  root.id = "root";
  document.body.appendChild(root);

  const div = document.querySelector('#root');
  const ul = document.createElement('ul');
  div.appendChild (ul);
  
  books.forEach ((book) => {
    try {
      if (!book.author) {
        throw new Error (`Data is not complete, author's name is empty!`);
      } else if (!book.name) {
        throw new Error (`Data is not complete, book's name is empty!`);
      } else if (!book.price) {
        throw new Error (`Data is not complete, price is empty!`);
      } else {
        const li = document.createElement('li');
        li.innerHTML = `${book.author} - `+
          `"${book.name}"`.italics()+
          `<br>Цена: ${book.price} UAH`;

        ul.appendChild(li);
      }
    } catch (error) {
      console.error(error);
    }
  });
};

addList(books);
